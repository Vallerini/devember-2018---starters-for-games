﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "item", menuName = "New Item")]
public class ItemObject : ScriptableObject 
{

	#region FIELDS

    public Sprite inventoryImage;

	#endregion

	#region UNITY CALLBACKS

	#endregion

	#region METHODS

	#endregion

}
