﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IPickUppable
{
    void PickUp(PlayerInventory inventory);
}
