﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour 
{

    #region FIELDS

    public GameObject inventoryHud;
    public GameObject inventoryContent;
    public GameObject inventorySlot;
    public KeyCode openCloseInvKey;

    bool isOpen;

    #endregion

    #region UNITY CALLBACKS

    void Awake()
    {
        FindObjectOfType<PlayerInventory>().UpdateInventory += RefreshInventory;
    }

    void Update()
    {
        if(Input.GetKeyDown(openCloseInvKey))
        {
            OpenCloseInventory();
        }
    }

    #endregion

    #region METHODS

    void OpenCloseInventory()
    {
        if(isOpen)
        {
            inventoryHud.SetActive(false);
            isOpen = false;
        }
        else
        {
            inventoryHud.SetActive(true);
            isOpen = true;
        }
    }

    void RefreshInventory(List<ItemObject> items)
    {
        foreach(Transform t in inventoryContent.GetComponentInChildren<Transform>())
        {
            Destroy(t.gameObject);
        }

        foreach(ItemObject io in items)
        {
            GameObject currSlot = Instantiate(inventorySlot, inventoryContent.transform);

            currSlot.GetComponent<Image>().sprite = io.inventoryImage;
        }

        RectTransform contentSize = inventoryContent.GetComponent<RectTransform>();
        GridLayoutGroup contentGrid = inventoryContent.GetComponent<GridLayoutGroup>();

        float xSize = contentGrid.padding.left + contentGrid.padding.right + 
            (contentGrid.cellSize.x * contentGrid.constraintCount) + (contentGrid.spacing.x * (contentGrid.constraintCount - 1)) ;
        float ySize = contentGrid.cellSize.y + contentGrid.padding.top + contentGrid.padding.bottom + 
            (contentGrid.cellSize.y * (int)(items.Count / contentGrid.constraintCount) + (contentGrid.spacing.y + (items.Count - 1)));

        contentSize.sizeDelta = new Vector2(xSize, ySize);
    }

    #endregion

}
