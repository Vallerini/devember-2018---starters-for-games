﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class OnClickMovementInv : MonoBehaviour
{

    #region FIELDS

    [Tooltip("If this is true a click on the HUD stops the movement of the player")]
    public bool hudBlocksMovement;

    NavMeshAgent agent;

    #endregion

    #region UNITY CALLBACKS

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        CheckClick();
    }

    #endregion

    #region METHODS

    void CheckClick()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (!(hudBlocksMovement && EventSystem.current.IsPointerOverGameObject()))
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (Physics.Raycast(ray, out hit, 100))
                {
                    agent.SetDestination(hit.point);
                }
            }
        }
    }

    #endregion

}
