﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTopDown2D : MonoBehaviour
{

    #region FIELDS

    enum Direction
    {
        UP,
        UPRIGHT,
        RIGHT,
        DOWNRIGHT,
        DOWN,
        DOWNLEFT,
        LEFT,
        UPLEFT,
    }

    public float maxSpeed;
    public float acceleration;
    public float deceleration;

    public KeyCode moveUp;
    public KeyCode moveDown;
    public KeyCode moveLeft;
    public KeyCode moveRight;

    float currSpeed = 0;
    int vertSpd = 0;
    int horizSpd = 0;
    Vector2 directionVect = Vector2.zero;

    #endregion

    #region UNITY CALLBACKS

    void FixedUpdate()
    {
        Move();
    }

    #endregion

    #region METHODS

    void Move()
    {
        vertSpd = 0;
        horizSpd = 0;

        if(Input.GetKey(moveUp))
        {
            vertSpd++;
        }
        if (Input.GetKey(moveDown))
        {
            vertSpd--;
        }
        if (Input.GetKey(moveLeft))
        {
            horizSpd--;
        }
        if (Input.GetKey(moveRight))
        {
            horizSpd++;
        }

        if(vertSpd == 0 && horizSpd == 0 && currSpeed != 0)
        {
            Decelerate();
        }
        else
        {
            CalculateDirection();
            Accelerate();
        }

        ApplyMovement();
    }

    void Decelerate()
    {
        currSpeed -= deceleration;

        if(currSpeed < deceleration)
        {
            currSpeed = 0;
        }
    }

    void Accelerate()
    {
        currSpeed += acceleration;

        if(currSpeed > maxSpeed)
        {
            currSpeed = maxSpeed;
        }
    }

    void ApplyMovement()
    {
        transform.position += (Vector3)directionVect.normalized * currSpeed;
    }

    void CalculateDirection()
    {
        directionVect = new Vector2(horizSpd, vertSpd);
        Vector3 spriteRotation = transform.eulerAngles;


        switch (horizSpd)
        {
            case 0:
                switch(vertSpd)
                {
                    case 1:
                        transform.eulerAngles = new Vector3(spriteRotation.x, spriteRotation.y, 0);
                        break;
                    case -1:
                        transform.eulerAngles = new Vector3(spriteRotation.x, spriteRotation.y, 180);
                        break;
                }
                break;
            case 1:
                switch (vertSpd)
                {
                    case 1:
                        transform.eulerAngles = new Vector3(spriteRotation.x, spriteRotation.y, 315);
                        break;
                    case -1:
                        transform.eulerAngles = new Vector3(spriteRotation.x, spriteRotation.y, 225);
                        break;
                    case 0:
                        transform.eulerAngles = new Vector3(spriteRotation.x, spriteRotation.y, 270);
                        break;
                }
                break;
            case -1:
                switch (vertSpd)
                {
                    case 1:
                        transform.eulerAngles = new Vector3(spriteRotation.x, spriteRotation.y, 45);
                        break;
                    case -1:
                        transform.eulerAngles = new Vector3(spriteRotation.x, spriteRotation.y, 135);
                        break;
                    case 0:
                        transform.eulerAngles = new Vector3(spriteRotation.x, spriteRotation.y, 90);
                        break;
                }
                break;
        }
    }

    #endregion

}
