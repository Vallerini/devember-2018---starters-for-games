﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SailControl : MonoBehaviour 
{

    #region FIELDS

    enum Directions
    {
        RIGHT,
        LEFT,
    }

    public KeyCode rotateRight;
    public KeyCode rotateLeft;

    public KeyCode accelerate;
    public KeyCode decelerate;

    public float sailRotationSpeed = 1;
    public float sailLenghtSpeed = 0.1f;
    public float currSailSpeed = 1;
    public float maxMinSailDegrees;
    public GameObject sailPole;
    public GameObject sail;
    public GameObject sailCenter;
    public GameObject sailTop;
    public GameObject sailController;
    public GameObject wind;

    float currSailLenght = 1;
    float currSailDegrees;
    float sailMaxScale;

    static float maxMinControllerDegrees = 55;

    #endregion

    #region UNITY CALLBACKS

    void Start()
    {
        sailMaxScale = sail.transform.localScale.y;
    }

    void Update()
    {
        if (Input.GetKey(rotateRight) && currSailDegrees < maxMinSailDegrees)
        {
            RotateSail(Directions.RIGHT);
        }

        if (Input.GetKey(rotateLeft) && currSailDegrees > -maxMinSailDegrees)
        {
            RotateSail(Directions.LEFT);
        }

        if (Input.GetKey(accelerate) && currSailLenght < 1)
        {
            Accelerate();
        }

        if (Input.GetKey(decelerate) && currSailLenght > 0)
        {
            Decelerate();
        }

        Vector3 windSailDifference = sailPole.transform.forward - wind.transform.forward;

        if (windSailDifference.magnitude < 0.5f)
        {
            currSailSpeed = currSailLenght * 5;
        }
        else if (windSailDifference.magnitude < 1f)
        {
            currSailSpeed = currSailLenght * 3;
        }
        else
        {
            currSailSpeed = currSailLenght;
        }

    }

    #endregion

    #region METHODS

    void Accelerate()
    {
        currSailLenght += sailLenghtSpeed;
        if (currSailLenght > 1)
        {
            currSailLenght = 1;
        }
        ModifySail();
    }

    void Decelerate()
    {
        currSailLenght -= sailLenghtSpeed;
        if(currSailLenght < 0)
        {
            currSailLenght = 0;
        }
        ModifySail();
    }

    void ModifySail()
    {
        //this can be removed with a good mesh
        sail.transform.localScale = new Vector3(sail.transform.localScale.x, Mathf.Lerp(0.05f, sailMaxScale, currSailLenght), sail.transform.localScale.z);
        sail.transform.localPosition = new Vector3(sail.transform.localPosition.x, Mathf.Lerp(sailTop.transform.localPosition.y, sailCenter.transform.localPosition.y, currSailLenght), sail.transform.localPosition.z);
    }

    void RotateSail(Directions dir)
    {
        if (dir == Directions.RIGHT)
        {
            currSailDegrees += sailRotationSpeed;
        }
        if (dir == Directions.LEFT)
        {
            currSailDegrees -= sailRotationSpeed;
        }

        if (currSailDegrees < 0.5f && currSailDegrees > -0.5f)
        {
            currSailDegrees = 0;
        }

        float sailControllerRotation = (((maxMinControllerDegrees * 2) * (currSailDegrees + maxMinSailDegrees)) / (maxMinSailDegrees * 2)) - maxMinControllerDegrees;
        // x : (maxminContDeg * 2) = (currSailDegrees + maxMinSailDegrees) : (maxMinSailDegrees * 2)  ----- (all - maxminContDeg)

        sailPole.transform.localEulerAngles = new Vector3(sailPole.transform.eulerAngles.x, currSailDegrees, sailPole.transform.eulerAngles.z);
        sailController.transform.eulerAngles = new Vector3(sailControllerRotation, sailController.transform.eulerAngles.y, sailController.transform.eulerAngles.z);
    }

    #endregion

}
