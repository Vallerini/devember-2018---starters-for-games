﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelControl : MonoBehaviour 
{

    #region FIELDS

    enum Directions
    {
        RIGHT,
        LEFT,
    }

    public KeyCode moveRight;
    public KeyCode moveLeft;

    public float wheelRotationSpeed = 2;
    [Tooltip("it goes from -1 to 1")]
    public float currRotation;
    public float maxWheelRotations = 1;
    public GameObject wheel;

    float maxMinDegrees;
    float currWheelDegrees;

    #endregion

    #region UNITY CALLBACKS

    void Start()
    {
        maxMinDegrees = 360 * maxWheelRotations; 
    }

    void Update()
    {
        if (Input.GetKey(moveRight) && currWheelDegrees < maxMinDegrees)
        {
            RotateWheel(Directions.RIGHT);
        }

        if (Input.GetKey(moveLeft) && currWheelDegrees > -maxMinDegrees)
        {
            RotateWheel(Directions.LEFT);
        }
    }

    #endregion

    #region METHODS

    void RotateWheel(Directions dir)
    {
        if(dir == Directions.RIGHT)
        {
            currWheelDegrees += wheelRotationSpeed;
        }
        if (dir == Directions.LEFT)
        {
            currWheelDegrees -= wheelRotationSpeed;
        }

        if(currWheelDegrees < 1.5f && currWheelDegrees > -1.5f)
        {
            currWheelDegrees = 0;
        }

        wheel.transform.eulerAngles = new Vector3(wheel.transform.eulerAngles.x, wheel.transform.eulerAngles.y, currWheelDegrees);

        currRotation = (((currWheelDegrees + maxMinDegrees) * 2) / (maxMinDegrees * 2)) - 1;
        // x : 2 = (currWheelDegrees + maxMinDegrees) : (maxMinDegrees * 2) ------- (I find from 0 to 2 then i do -1)
    }

    #endregion

}
