﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll3rdPers : MonoBehaviour
{

    #region FIELDS

    public GameObject target;
    [Tooltip("1 for normal")]
    public float horizSensibility = 1;
    [Tooltip("1 for normal")]
    public float vertSensibility = 1;

    #endregion

    #region UNITY CALLBACKS

    void Update()
    {
        FollowTarget();
        RotateCamera();
    }

    #endregion

    #region METHODS

    void RotateCamera()
    {
        float hMovement = Input.GetAxis("Mouse X") * horizSensibility;
        float vMovement = Input.GetAxis("Mouse Y") * -vertSensibility;

        transform.Rotate(Vector3.up, hMovement, Space.World);
        transform.Rotate(Vector3.right, vMovement, Space.Self);
    }

    void FollowTarget()
    {
        transform.position = target.transform.position;
    }

    #endregion

}
