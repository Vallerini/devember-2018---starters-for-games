﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement3rdPerson : MonoBehaviour
{

    #region FIELDS

    enum Direction
    {
        FRONT,
        BACK,
        LEFT,
        RIGHT,
        FRONTAL,
        LATERAL,
    }

    public KeyCode freeLook;
    public KeyCode moveForward;
    public KeyCode moveBackward;
    public KeyCode moveRight;
    public KeyCode moveLeft;

    public GameObject cameraController; 

    public float maxForwardSpeed;
    public float maxBackwardSpeed;
    public float maxLateralSpeed;
    public float acceleration;
    public float deceleration;

    float currFrontalSpeed = 0;
    float currLateralSpeed = 0;
    float currVerticalSpeed = 0;
    int frontMove = 0;
    int laterMove = 0;

    bool isFreeLooking;

    #endregion

    #region UNITY CALLBACKS

    void Update()
    {
        frontMove = 0;
        laterMove = 0;

        #region MOVEMENT INPUTS

        if (Input.GetKey(moveForward))
        {
            frontMove++;
        }
        if (Input.GetKey(moveBackward))
        {
            frontMove--;
        }
        if (Input.GetKey(moveRight))
        {
            laterMove++;
        }
        if (Input.GetKey(moveLeft))
        {
            laterMove--;
        }

        #endregion

        #region CAMERA INPUTS

        if (Input.GetKeyDown(freeLook))
        {
            isFreeLooking = true;
        }
        if (Input.GetKeyUp(freeLook))
        {
            isFreeLooking = false;
        }

        #endregion

        #region SPEED CHANGES

        if (frontMove == 0 && currFrontalSpeed != 0)
        {
            Decelerate(Direction.FRONTAL);
        }
        else if (frontMove == 1)
        {
            Accelerate(Direction.FRONT);
        }
        else if (frontMove == -1)
        {
            Accelerate(Direction.BACK);
        }

        if (laterMove == 0 && currLateralSpeed != 0)
        {
            Decelerate(Direction.LATERAL);
        }
        else if (laterMove == 1)
        {
            Accelerate(Direction.RIGHT);
        }
        else if (laterMove == -1)
        {
            Accelerate(Direction.LEFT);
        }

        #endregion

        if (!isFreeLooking)
        {
            SetRotation();
        }
    }

    void FixedUpdate()
    {
        Move();
    }

    #endregion

    #region METHODS

    void Move()
    {
        Vector3 xMove = transform.right * currLateralSpeed;
        Vector3 yMove = transform.up * currVerticalSpeed;
        Vector3 zMove = transform.forward * currFrontalSpeed;
        Vector3 movementVec = xMove + yMove + zMove;

        transform.position += movementVec;
    }

    void Decelerate(Direction dir)
    {
        switch (dir)
        {
            case Direction.FRONTAL:
                currFrontalSpeed = LowerToZero(currFrontalSpeed, deceleration);
                break;
            case Direction.LATERAL:
                currLateralSpeed = LowerToZero(currLateralSpeed, deceleration);
                break;
        }
    }

    void Accelerate(Direction dir)
    {
        switch (dir)
        {
            case Direction.FRONT:
                currFrontalSpeed = RaiseToMax(currFrontalSpeed, maxForwardSpeed, acceleration);
                break;
            case Direction.BACK:
                currFrontalSpeed = RaiseToMax(currFrontalSpeed, -maxBackwardSpeed, -acceleration);
                break;
            case Direction.RIGHT:
                currLateralSpeed = RaiseToMax(currLateralSpeed, maxLateralSpeed, acceleration);
                break;
            case Direction.LEFT:
                currLateralSpeed = RaiseToMax(currLateralSpeed, -maxLateralSpeed, -acceleration);
                break;
        }
    }

    void SetRotation()
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, cameraController.transform.eulerAngles.y, transform.eulerAngles.z);
    }

    float LowerToZero(float value, float deceleration)
    {
        float returnedVal = 0;

        returnedVal = value - (Mathf.Sign(value) * deceleration);
        if (returnedVal < deceleration && returnedVal > -deceleration)
        {
            returnedVal = 0;
        }

        return returnedVal;
    }

    float RaiseToMax(float value, float max, float acceleration)
    {
        float returnedVal = 0;

        returnedVal = value + acceleration;
        if((max > 0 && returnedVal > max) || (max < 0 && returnedVal < max))
        {
            returnedVal = max;
        }

        return returnedVal;
    }

    #endregion

}
