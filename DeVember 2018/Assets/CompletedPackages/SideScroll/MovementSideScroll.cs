﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSideScroll : MonoBehaviour
{

    #region FIELDS

    enum Direction
    {
        RIGHT,
        LEFT,
    }

    public KeyCode moveLeft;
    public KeyCode moveRight;
    public KeyCode jump;

    public float maxSpeed;
    public float jumpStrenght;
    public float acceleration;
    public float deceleration;

    [HideInInspector]
    public bool isOnGround;

    Rigidbody2D rb;
    float currSpeed = 0;
    float horizSpd = 0;

    #endregion

    #region UNITY CALLBACKS

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();    
    }

    void Update()
    {
        horizSpd = 0;

        #region INPUTS RECEIVER

        if (Input.GetKey(moveLeft))
        {
            horizSpd--;
        }
        if (Input.GetKey(moveRight))
        {
            horizSpd++;
        }

        if (Input.GetKeyDown(jump) && isOnGround)
        {
            Jump();
        }

        #endregion
    }

    void FixedUpdate()
    {
        Move();
    }

    #endregion

    #region METHODS

    void Move()
    {

        if (horizSpd == 0 && currSpeed != 0)
        {
            Decelerate();
        }
        else if (horizSpd > 0)
        {
            Accelerate(Direction.RIGHT);
        }
        else if (horizSpd < 0)
        {
            Accelerate(Direction.LEFT);
        }

        ApplyMovement();
    }

    void ApplyMovement()
    {
        transform.position += Vector3.right * currSpeed;
    }

    void Jump()
    {
        rb.AddForce(Vector3.up * jumpStrenght * 100);
        isOnGround = false;
    }

    void Accelerate(Direction dir)
    {
        if(dir == Direction.LEFT)
        {
            currSpeed -= acceleration;

            if (currSpeed < -maxSpeed)
            {
                currSpeed = -maxSpeed;
            }
        }
        else if (dir == Direction.RIGHT)
        {
            currSpeed += acceleration;

            if (currSpeed > maxSpeed)
            {
                currSpeed = maxSpeed;
            }
        }
    }

    void Decelerate()
    {
        currSpeed -= Mathf.Sign(currSpeed) * deceleration;

        if (currSpeed < deceleration && currSpeed > - deceleration)
        {
            currSpeed = 0;
        }
    }

    #endregion

}
